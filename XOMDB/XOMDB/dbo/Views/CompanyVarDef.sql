﻿CREATE VIEW dbo.CompanyVarDef
AS
SELECT c.CompanyID, vd.VKey, vd.Variable, vd.UOM, vd.LongText, vd.DataType, vd.Decimals, vd.IsMetric, vd.SectionCode
	, tsModified = CASE WHEN cv.tsAdded > vd.tsModified THEN cv.tsAdded ELSE vd.tsModified END
	, cv.SuppressData
FROM dbo.Companies c INNER JOIN dbo.CompanyVKeys cv ON cv.CompanyKey = c.CompanyKey
INNER JOIN dbo.VarDef vd ON vd.VKey = cv.VKey

