﻿
CREATE VIEW [dbo].[OutputFileSheetColumns]
AS
SELECT s.FSKey, f.FKey, f.Filename, s.SheetName, c.ColumnNo, c.PGKey, c.PPGKey, c.Refnum, c.UnitID, s.SheetNo, f.Study, f.StudyYear, f.FileType, f.Currency
FROM dbo.OutputFiles f 
	INNER JOIN dbo.OutputSheets s ON s.FKey = f.FKey
	INNER JOIN dbo.OutputSheetColumns c ON c.FSKey = s.FSKey
	

