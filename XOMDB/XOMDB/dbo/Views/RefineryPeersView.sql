﻿CREATE VIEW RefineryPeersView
AS
SELECT rp.RefineryPeerKey, rp.FacilityPGKey, FacilityRefnum = a.StudyRefnum, FacilityMethodology = a.Methodology, FacilityPricingScenario = a.PricingScenario, FacilitySourceDatabase = a.SourceDatabase,
rp.TargetPGKey, TargetRefnum =  b.StudyRefnum, TargetMethodology = b.Methodology, TargetPricingScenario = b.PricingScenario, TargetSourceDatabase = b.SourceDatabase
FROM RefineryPeers rp INNER JOIN PeerGroupDef a ON a.PGKey = rp.FacilityPGKey
 INNER JOIN PeerGroupDef b ON b.PGKey = rp.TargetPGKey
WHERE RefineryPeerKey IN (
SELECT RefineryPeerKey FROM RefineryVsPeer WHERE tsModified > '1/1/16')
