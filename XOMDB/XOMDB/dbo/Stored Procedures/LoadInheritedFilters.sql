﻿CREATE PROC [dbo].[LoadInheritedFilters] AS

; WITH Filters AS (SELECT FSKey, RowNo, DBTableName, DBFilter FROM OutputSheetRows WHERE DBFilter <> '')
UPDATE r SET DBFilter = f.DBFilter
FROM Filters f
INNER JOIN OutputSheetRows r ON r.FSKey = f.FSKey AND f.DBTableName = r.DBTableName
WHERE ISNULL(r.DBFilter,'') = '' AND f.RowNo < r.RowNo AND f.RowNo = (SELECT MAX(RowNo) FROM Filters x WHERE x.FSKey = r.FSKey AND x.DBTableName = r.DBTableName AND x.RowNo < r.RowNo)

SELECT DISTINCT DBFilter
FROM dbo.OutputSheetRows r
WHERE DBFilter <> '' AND NOT EXISTS (SELECT * FROM dbo.OutputFilters f WHERE f.MasterFileFilter = r.DBFilter)

