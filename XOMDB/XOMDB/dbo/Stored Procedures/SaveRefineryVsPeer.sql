﻿

CREATE PROCEDURE [dbo].[SaveRefineryVsPeer](@RefineryVsPeer dbo.RefineryVsPeerTable READONLY)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @Compare TABLE (
	[RefineryPeerKey] [int] NOT NULL,
	[VKey] [int] NOT NULL,
	[Currency] [varchar](4) NOT NULL,
	[numberValue] [float] NULL,
	[CurrentVKey] [int] NULL,
	[CurrentNumberValue] [float] NULL,
	[UpdateAction] [char](1) NULL,
	[Decimals] tinyint NOT NULL
	)
	
	INSERT @Compare (RefineryPeerKey, VKey, Currency, numberValue, CurrentVKey, CurrentNumberValue, Decimals)
	SELECT tmp.RefineryPeerKey, v.VKey, tmp.Currency, tmp.numberValue, cd.VKey, cd.numberValue, v.Decimals
	FROM @RefineryVsPeer tmp INNER JOIN dbo.VarDef v ON v.VKey = tmp.VKey OR (tmp.VKey = 0 AND v.Variable = tmp.Variable) 
	LEFT JOIN dbo.RefineryVsPeer cd ON cd.RefineryPeerKey = tmp.RefineryPeerKey AND cd.Currency = tmp.Currency AND cd.VKey = v.VKey 

	UPDATE @Compare SET UpdateAction = 'A' WHERE CurrentVKey IS NULL
	UPDATE @Compare SET UpdateAction = 'U' WHERE UpdateAction IS NULL AND ((numberValue IS NULL AND CurrentNumberValue IS NOT NULL) OR (numberValue IS NOT NULL AND CurrentNumberValue IS NULL))
	UPDATE @Compare SET UpdateAction = 'U' WHERE UpdateAction IS NULL AND (numberValue IS NOT NULL AND CurrentNumberValue IS NOT NULL) AND (ROUND(numberValue, Decimals+2) <> ROUND(CurrentNumberValue, Decimals+2))

	UPDATE rd
	SET numberValue =ROUND(tmp.numberValue, Decimals+2)
	FROM dbo.RefineryVsPeer rd INNER JOIN @Compare tmp ON tmp.RefineryPeerKey = rd.RefineryPeerKey AND tmp.VKey = rd.VKey AND tmp.Currency = rd.Currency
	WHERE tmp.UpdateAction = 'U' OR tmp.UpdateAction IS NULL

	INSERT dbo.RefineryVsPeer (RefineryPeerKey, VKey, Currency, numberValue, tsModified, tsVerified)
	SELECT tmp.RefineryPeerKey, tmp.VKey, tmp.Currency, ROUND(tmp.numberValue, Decimals+2), GETDATE(), GETDATE()
	FROM @Compare tmp 
	WHERE tmp.UpdateAction = 'A'

	SET NOCOUNT OFF

END


