﻿CREATE PROC spDeletePGKey(@PGKey int)
AS
DELETE FROM ProcessVsPeer WHERE ProcessPeerKey IN (SELECT ProcessPeerKey FROM ProcessPeers WHERE FacilityPPGKey IN (SELECT PPGKey FROM ProcessPeerGroupDef WHERE PGKey = @PGKey))
DELETE FROM ProcessVsPeer WHERE ProcessPeerKey IN (SELECT ProcessPeerKey FROM ProcessPeers WHERE TargetPPGKey IN (SELECT PPGKey FROM ProcessPeerGroupDef WHERE PGKey = @PGKey))
DELETE FROM ProcessPeers WHERE FacilityPPGKey IN (SELECT PPGKey FROM ProcessPeerGroupDef WHERE PGKey = @PGKey)
DELETE FROM ProcessPeers WHERE TargetPPGKey IN (SELECT PPGKey FROM ProcessPeerGroupDef WHERE PGKey = @PGKey)
DELETE FROM ProcessData WHERE PPGKey IN (SELECT PPGKey FROM ProcessPeerGroupDef WHERE PGKey = @PGKey)
DELETE FROM ProcessPeerGroupDef WHERE PGKey = @PGKey

DELETE FROM RefineryVsPeer WHERE RefineryPeerKey IN (SELECT RefineryPeerKey FROM RefineryPeers WHERE FacilityPGKey = @PGKey)
DELETE FROM RefineryVsPeer WHERE RefineryPeerKey IN (SELECT RefineryPeerKey FROM RefineryPeers WHERE TargetPGKey = @PGKey)
DELETE FROM RefineryPeers WHERE FacilityPGKey = @PGKey OR TargetPGKey = @PGKey
DELETE FROM RefineryData WHERE PGKey = @PGKey
DELETE FROM PeerGroupDef WHERE PGKey = @PGKey

