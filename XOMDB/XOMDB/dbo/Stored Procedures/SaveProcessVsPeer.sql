﻿

CREATE PROCEDURE [dbo].[SaveProcessVsPeer](@ProcessVsPeer dbo.ProcessVsPeerTable READONLY)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @Compare TABLE (
	[ProcessPeerKey] [int] NOT NULL,
	[VKey] [int] NOT NULL,
	[Currency] [varchar](4) NOT NULL,
	[numberValue] [float] NULL,
	[CurrentVKey] [int] NULL,
	[CurrentNumberValue] [float] NULL,
	[UpdateAction] [char](1) NULL,
	[Decimals] tinyint NOT NULL
	)
	
	INSERT @Compare (ProcessPeerKey, VKey, Currency, numberValue, CurrentVKey, CurrentNumberValue, Decimals)
	SELECT tmp.ProcessPeerKey, v.VKey, tmp.Currency, tmp.numberValue, cd.VKey, cd.numberValue, v.Decimals
	FROM @ProcessVsPeer tmp INNER JOIN dbo.VarDef v ON v.VKey = tmp.VKey OR (tmp.VKey = 0 AND v.Variable = tmp.Variable) 
	LEFT JOIN dbo.ProcessVsPeer cd ON cd.ProcessPeerKey = tmp.ProcessPeerKey AND cd.Currency = tmp.Currency AND cd.VKey = v.VKey 

	UPDATE @Compare SET UpdateAction = 'A' WHERE CurrentVKey IS NULL
	UPDATE @Compare SET UpdateAction = 'U' WHERE UpdateAction IS NULL AND ((numberValue IS NULL AND CurrentNumberValue IS NOT NULL) OR (numberValue IS NOT NULL AND CurrentNumberValue IS NULL))
	UPDATE @Compare SET UpdateAction = 'U' WHERE UpdateAction IS NULL AND (numberValue IS NOT NULL AND CurrentNumberValue IS NOT NULL) AND (ROUND(numberValue, Decimals+2) <> ROUND(CurrentNumberValue, Decimals+2))

	UPDATE rd
	SET numberValue =ROUND(tmp.numberValue, Decimals+2)
	FROM dbo.ProcessVsPeer rd INNER JOIN @Compare tmp ON tmp.ProcessPeerKey = rd.ProcessPeerKey AND tmp.VKey = rd.VKey AND tmp.Currency = rd.Currency
	WHERE tmp.UpdateAction = 'U' OR tmp.UpdateAction IS NULL

	INSERT dbo.ProcessVsPeer (ProcessPeerKey, VKey, Currency, numberValue, tsModified, tsVerified)
	SELECT tmp.ProcessPeerKey, tmp.VKey, tmp.Currency, ROUND(tmp.numberValue, Decimals+2), GETDATE(), GETDATE()
	FROM @Compare tmp 
	WHERE tmp.UpdateAction = 'A'

	SET NOCOUNT OFF

END


