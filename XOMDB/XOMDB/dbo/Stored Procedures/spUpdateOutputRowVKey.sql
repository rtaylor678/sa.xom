﻿CREATE PROC spUpdateOutputRowVKey
AS
BEGIN
	UPDATE f
	SET VKey = vd.VKey
	FROM dbo.OutputFileSheetRows f 
	LEFT JOIN dbo.OutputFilters rf ON rf.MasterFileFilter = f.DBFilter
	LEFT JOIN (dbo.VariableDataSource vds INNER JOIN dbo.VarDef vd ON vd.Variable = vds.Variable) ON vds.TableName = f.DBTableName AND vds.ColumnName = f.DBColumnName AND ISNULL(vds.Filter,'') = ISNULL(rf.FilterNoCurrencyScenario,'')
	WHERE f.FileType IN ('Output','Trend') AND ISNULL(f.DBTableName,'') <> '' AND ISNULL(f.DBColumnName,'') <> '' AND f.VKey IS NULL
	
	UPDATE f SET VKey = v.VKey
	FROM dbo.OutputFileSheetRows f
	INNER JOIN Refining.Report.UnitPropertyMap m ON (f.DBTableName = 'ReportPM_'+m.Process OR (m.Process = 'General' AND f.DBTableName IN ('ReportPM','ReportPMGeneral','ReportPMCost','ReportPM_Metric')))AND f.DBColumnName = m.ReportTableColumn
	INNER JOIN dbo.VarDef v ON v.Variable = m.PropertyName
	WHERE f.FileType = 'Process' AND ISNULL(f.DBTableName,'') <> '' AND ISNULL(f.DBColumnName,'') <> '' AND f.VKey IS NULL
END
