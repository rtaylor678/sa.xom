﻿
CREATE PROCEDURE [dbo].[SaveProcessData](@ProcessData dbo.ProcessDataTable READONLY)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @Compare TABLE (
	[PPGKey] [int] NOT NULL,
	[VKey] [int] NOT NULL,
	[Currency] [varchar](4) NOT NULL,
	[numberValue] [float] NULL,
	[textValue] [nvarchar](max) NULL,
	[dateValue] [smalldatetime] NULL,
	[CurrentVKey] [int] NULL,
	[CurrentNumberValue] [float] NULL,
	[CurrentTextValue] [nvarchar](max) NULL,
	[CurrentDateValue] [smalldatetime] NULL,
	[UpdateAction] [char](1) NULL,
	[DataType] [varchar](10) NOT NULL,
	[Decimals] tinyint NOT NULL
	)
	
	INSERT @Compare (PPGKey, VKey, Currency, numberValue, textValue, dateValue, CurrentVKey, CurrentNumberValue, CurrentTextValue, CurrentDateValue, DataType, Decimals)
	SELECT tmp.PPGKey, v.VKey, tmp.Currency, tmp.numberValue, tmp.textValue, tmp.dateValue, cd.VKey, cd.numberValue, cd.textValue, cd.dateValue, v.DataType, v.Decimals
	FROM @ProcessData tmp INNER JOIN dbo.VarDef v ON v.VKey = tmp.VKey OR (tmp.VKey = 0 AND v.Variable = tmp.Variable) 
	LEFT JOIN dbo.ProcessData cd ON cd.PPGKey = tmp.PPGKey AND cd.Currency = tmp.Currency AND cd.VKey = v.VKey 

	UPDATE @Compare SET UpdateAction = 'A' WHERE CurrentVKey IS NULL
	UPDATE @Compare SET UpdateAction = 'U' WHERE UpdateAction IS NULL AND DataType = 'number' AND ((numberValue IS NULL AND CurrentNumberValue IS NOT NULL) OR (numberValue IS NOT NULL AND CurrentNumberValue IS NULL))
	UPDATE @Compare SET UpdateAction = 'U' WHERE UpdateAction IS NULL AND DataType = 'number' AND (numberValue IS NOT NULL AND CurrentNumberValue IS NOT NULL) AND (ROUND(numberValue, Decimals+2) <> ROUND(CurrentNumberValue, Decimals+2))
	UPDATE @Compare SET UpdateAction = 'U' WHERE UpdateAction IS NULL AND DataType = 'text' AND ((textValue IS NULL AND CurrentTextValue IS NOT NULL) OR (textValue IS NOT NULL AND CurrentTextValue IS NULL))
	UPDATE @Compare SET UpdateAction = 'U' WHERE UpdateAction IS NULL AND DataType = 'text' AND (textValue IS NOT NULL AND CurrentTextValue IS NOT NULL) AND (textValue <> CurrentTextValue)
	UPDATE @Compare SET UpdateAction = 'U' WHERE UpdateAction IS NULL AND DataType = 'date' AND ((dateValue IS NULL AND CurrentDateValue IS NOT NULL) OR (dateValue IS NOT NULL AND CurrentDateValue IS NULL))
	UPDATE @Compare SET UpdateAction = 'U' WHERE UpdateAction IS NULL AND DataType = 'date' AND (dateValue IS NOT NULL AND CurrentDateValue IS NOT NULL) AND (dateValue <> CurrentDateValue)

	UPDATE rd
	SET numberValue = CASE WHEN DataType = 'number' THEN ROUND(tmp.numberValue, Decimals+2) ELSE NULL END
		, textValue = CASE WHEN DataType = 'text' THEN tmp.textValue ELSE NULL END
		, dateValue = CASE WHEN DataType = 'date' THEN tmp.dateValue ELSE NULL END
		--, tsModified = GETDATE(), tsVerified = GETDATE()
	FROM dbo.ProcessData rd INNER JOIN @Compare tmp ON tmp.PPGKey = rd.PPGKey AND tmp.VKey = rd.VKey AND tmp.Currency = rd.Currency
	WHERE tmp.UpdateAction = 'U' OR tmp.UpdateAction IS NULL

	INSERT dbo.ProcessData (PPGKey, VKey, Currency, numberValue, textValue, dateValue, tsModified, tsVerified)
	SELECT tmp.PPGKey, tmp.VKey, tmp.Currency, ROUND(tmp.numberValue, Decimals+2), tmp.textValue, tmp.dateValue, GETDATE(), GETDATE()
	FROM @Compare tmp 
	WHERE tmp.UpdateAction = 'A'

	SET NOCOUNT OFF

END

