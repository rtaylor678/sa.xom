﻿CREATE TYPE [dbo].[ProcessDataTable] AS TABLE (
    [PPGKey]      INT            NOT NULL,
    [VKey]        INT            DEFAULT ((0)) NOT NULL,
    [Variable]    VARCHAR (255)  DEFAULT ('') NOT NULL,
    [Currency]    VARCHAR (4)    DEFAULT ('USD') NOT NULL,
    [numberValue] FLOAT (53)     NULL,
    [textValue]   NVARCHAR (MAX) NULL,
    [dateValue]   SMALLDATETIME  NULL,
    PRIMARY KEY CLUSTERED ([PPGKey] ASC, [VKey] ASC, [Variable] ASC, [Currency] ASC));

