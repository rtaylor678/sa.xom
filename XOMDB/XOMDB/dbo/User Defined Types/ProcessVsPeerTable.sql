﻿CREATE TYPE [dbo].[ProcessVsPeerTable] AS TABLE (
    [ProcessPeerKey] INT           NOT NULL,
    [VKey]           INT           DEFAULT ((0)) NOT NULL,
    [Variable]       VARCHAR (255) DEFAULT ('') NOT NULL,
    [Currency]       VARCHAR (4)   DEFAULT ('USD') NOT NULL,
    [numberValue]    REAL          NULL,
    PRIMARY KEY CLUSTERED ([ProcessPeerKey] ASC, [VKey] ASC, [Variable] ASC, [Currency] ASC));

