﻿

CREATE FUNCTION [dbo].[GetPeerGroupDefsNew](@CompanyID varchar(15), @dtModifiedSince datetime)
RETURNS @results TABLE (PGKey int NOT NULL, Study varchar(10) NOT NULL, StudyYear smallint NOT NULL, Methodology varchar(10) NOT NULL
	, RegionCode varchar(5) NOT NULL, MajorGroup varchar(50) NOT NULL, SubGroup varchar(50) NOT NULL, PricingScenario varchar(50) NOT NULL
	, LongText nvarchar(MAX) NOT NULL, PublishEuro bit NOT NULL, PGType varchar(10) NOT NULL, tsModified datetime NOT NULL)
AS 
BEGIN
	INSERT @results (PGKey, Study, StudyYear, Methodology, RegionCode, MajorGroup, SubGroup, PricingScenario, LongText, PublishEuro, PGType, tsModified)
	SELECT pgd.PGKey, pgd.Study, pgd.StudyYear, pgd.Methodology, pgd.RegionCode, pgd.MajorGroup, pgd.SubGroup, pgd.PricingScenario, pgd.LongText, pgd.PublishEuro, pgd.PGType
		, tsModified = CASE WHEN pgd.tsModified > cpgk.tsAdded THEN pgd.tsModified ELSE cpgk.tsAdded END
	FROM dbo.CompanyPGKeys cpgk INNER JOIN dbo.PeerGroupDef pgd ON pgd.PGKey = cpgk.PGKey
	INNER JOIN dbo.Companies c ON c.CompanyKey = cpgk.CompanyKey
	WHERE c.CompanyID = @CompanyID AND (pgd.tsModified > ISNULL(@dtModifiedSince, '1/1/2000') OR cpgk.tsAdded > ISNULL(@dtModifiedSince, '1/1/2000'))

	RETURN
END
