﻿

CREATE FUNCTION [dbo].[GetFurnaceData](@CompanyID varchar(15), @dtModifiedSince datetime)
RETURNS TABLE AS
RETURN (
	SELECT fd.FPGKey, fd.VKey, fd.numberValue, fd.textValue, tsModified = CASE WHEN k.tsModified > fd.tsModified THEN k.tsModified ELSE fd.tsModified END
	FROM (SELECT DISTINCT pgd.Study, pgd.StudyYear, fpgd.ProcessID, fpgd.FPGKey, /*s.SectionCode,*/ pgd.PublishEuro, fpgd.tsModified
		FROM dbo.GetFurnacePeerGroupDefs(@CompanyID, NULL) fpgd
		INNER JOIN dbo.PeerGroupDef pgd ON pgd.PGKey = fpgd.PGKey 
		--INNER JOIN dbo.ProcessPeerGroupSections s ON s.Study = pgd.Study AND s.ProcessID = fpgd.ProcessID AND s.ProcessType = fpgd.ProcessType AND s.RegionCode = pgd.RegionCode AND s.MajorGroup = pgd.MajorGroup
		
		UNION
		
		SELECT DISTINCT pgd.Study, pgd.StudyYear, fpgd.ProcessID, fpgd.FPGKey, /*s.SectionCode,*/ pgd.PublishEuro, fpgd.tsModified
		FROM dbo.GetFurnacePeerGroupDefs(@CompanyID, NULL) fpgd INNER JOIN dbo.PeerGroupDef pgd ON pgd.PGKey = fpgd.PGKey
		--INNER JOIN dbo.ProcessPeerGroupSections s ON s.Study = pgd.Study --AND s.ProcessID = ppgd.ProcessID
	) k
	INNER JOIN (SELECT vd.VKey, vd.SectionCode, vd.tsModified FROM dbo.CompanyVarDef vd WHERE vd.CompanyID = @CompanyID AND vd.SuppressData = 0) v ON 1=1
	INNER JOIN dbo.FurnaceData fd ON fd.VKey = v.VKey AND fd.FPGKey = k.FPGKey
	WHERE fd.tsModified > ISNULL(@dtModifiedSince, '1/1/2000') OR k.tsModified > ISNULL(@dtModifiedSince, '1/1/2000') OR v.tsModified > ISNULL(@dtModifiedSince, '1/1/2000')
)


