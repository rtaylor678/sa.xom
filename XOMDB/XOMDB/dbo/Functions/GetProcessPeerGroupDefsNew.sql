﻿
CREATE FUNCTION [dbo].[GetProcessPeerGroupDefsNew](@CompanyID varchar(15), @dtModifiedSince datetime)
RETURNS TABLE
AS
RETURN (
	SELECT DISTINCT ppgd.PPGKey, ppgd.PGKey, ppgd.ProcessID, ppgd.ProcessType, ppgd.LongText, ppgd.Class, ppgd.SubClass, ppgd.PeerType, ppgd.RamGroup
		, tsModified = CASE WHEN ppgd.tsModified >= cpgk.tsAdded AND ppgd.tsModified >= csp.tsAdded THEN ppgd.tsModified
							WHEN cpgk.tsAdded >= ppgd.tsModified AND cpgk.tsAdded >= csp.tsAdded THEN cpgk.tsAdded
							WHEN csp.tsAdded >= ppgd.tsModified AND csp.tsAdded >= cpgk.tsAdded THEN csp.tsAdded
							ELSE ppgd.tsModified END
	FROM dbo.CompanyPGKeys cpgk INNER JOIN dbo.PeerGroupDef pgd ON pgd.PGKey = cpgk.PGKey
	INNER JOIN dbo.Companies c ON c.CompanyKey = cpgk.CompanyKey
	INNER JOIN dbo.ProcessPeerGroupDef ppgd ON ppgd.PGKey = cpgk.PGKey
	INNER JOIN dbo.CompanyStudyProcesses csp ON csp.CompanyID = c.CompanyID AND (csp.Study = pgd.Study OR (csp.Study = 'LIV' AND pgd.Study = 'LIVAll') OR (csp.Study = 'LIV' AND pgd.Study = 'LEV'))AND csp.StudyYear = pgd.StudyYear AND csp.ProcessID = ppgd.ProcessID
	WHERE c.CompanyID = @CompanyID AND (cpgk.tsAdded > ISNULL(@dtModifiedSince, '1/1/2000')
		OR	ppgd.tsModified > ISNULL(@dtModifiedSince, '1/1/2000')
		OR  csp.tsAdded > ISNULL(@dtModifiedSince, '1/1/2000')
		) /*
	UNION
	SELECT DISTINCT tpg.PPGKey, tpg.PGKey, tpg.ProcessID, tpg.ProcessType, tpg.LongText, tpg.Class, tpg.SubClass, tpg.PeerType, tpg.RamGroup, tpg.tsModified
	FROM dbo.GetPeerGroupDefs(@CompanyID, NULL) r INNER JOIN dbo.ProcessPeerGroupDef pgd ON r.PGKey = pgd.PGKey
	INNER JOIN dbo.ProcessPeers pp ON pp.FacilityPPGKey = pgd.PPGKey
	INNER JOIN dbo.ProcessPeerGroupDef tpg ON tpg.PPGKey = pp.TargetPPGKey
	WHERE tpg.tsModified > ISNULL(@dtModifiedSince, '1/1/2000') OR pp.tsModified > ISNULL(@dtModifiedSince, '1/1/2000') 
	*/
)



