﻿

CREATE FUNCTION [dbo].[GetFurnacePeerGroupDefs](@CompanyID varchar(15), @dtModifiedSince datetime)
RETURNS TABLE
AS
RETURN (
	SELECT DISTINCT fpg.FPGKey, ppgd.PPGKey, ppgd.PGKey, ppgd.ProcessID, ppgd.ProcessType, fpg.FurnaceService, fpg.LongText, fpg.HeaterNo
		, tsModified = CASE WHEN fpg.tsModified >= cpgk.tsAdded AND fpg.tsModified >= csp.tsAdded THEN fpg.tsModified
							WHEN ppgd.tsModified >= cpgk.tsAdded AND ppgd.tsModified >= csp.tsAdded THEN ppgd.tsModified
							WHEN cpgk.tsAdded >= ppgd.tsModified AND cpgk.tsAdded >= csp.tsAdded THEN cpgk.tsAdded
							WHEN csp.tsAdded >= ppgd.tsModified AND csp.tsAdded >= cpgk.tsAdded THEN csp.tsAdded
							ELSE ppgd.tsModified END
	FROM dbo.CompanyPGKeys cpgk INNER JOIN dbo.PeerGroupDef pgd ON pgd.PGKey = cpgk.PGKey
	INNER JOIN dbo.Companies c ON c.CompanyKey = cpgk.CompanyKey
	INNER JOIN dbo.ProcessPeerGroupDef ppgd ON ppgd.PGKey = cpgk.PGKey
	INNER JOIN dbo.CompanyStudyProcesses csp ON csp.CompanyID = c.CompanyID AND (csp.Study = pgd.Study OR (csp.Study = 'LIV' AND pgd.Study = 'LIVAll'))AND csp.StudyYear = pgd.StudyYear AND csp.ProcessID = ppgd.ProcessID
	INNER JOIN dbo.FurnacePeerGroupDef fpg ON fpg.PPGKey = ppgd.PPGKey
	WHERE c.CompanyID = @CompanyID AND (cpgk.tsAdded > ISNULL(@dtModifiedSince, '1/1/2000')
		OR	ppgd.tsModified > ISNULL(@dtModifiedSince, '1/1/2000')
		OR  csp.tsAdded > ISNULL(@dtModifiedSince, '1/1/2000')
		OR  fpg.tsModified > ISNULL(@dtModifiedSince, '1/1/2000')
		)
)




