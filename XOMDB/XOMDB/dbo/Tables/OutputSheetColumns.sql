﻿CREATE TABLE [dbo].[OutputSheetColumns] (
    [FSKey]       INT           NOT NULL,
    [ColumnNo]    INT           NOT NULL,
    [Refnum]      VARCHAR (50)  NOT NULL,
    [UnitID]      VARCHAR (50)  NULL,
    [ColumnWidth] REAL          NULL,
    [PGKey]       INT           NULL,
    [PPGKey]      INT           NULL,
    [tsModified]  SMALLDATETIME DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_OutputSheetColumns] PRIMARY KEY CLUSTERED ([FSKey] ASC, [ColumnNo] ASC)
);


GO

CREATE TRIGGER [dbo].[SetOutputSheetColumnsModifiedAfterInsert]
   ON  [dbo].[OutputSheetColumns]
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE OutputSheetColumns
	SET tsModified = getdate()
	FROM dbo.OutputSheetColumns OutputSheetColumns
	INNER JOIN inserted i ON i.FSKey = OutputSheetColumns.FSKey AND i.ColumnNo = OutputSheetColumns.ColumnNo
	
END


GO

CREATE TRIGGER [dbo].[SetOutputSheetColumnsModifiedAfterUpdate]
   ON  [dbo].[OutputSheetColumns]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	SELECT FSKey, ColumnNo INTO #keychanged 
	FROM inserted 
	WHERE NOT EXISTS (SELECT * FROM deleted WHERE deleted.FSKey = inserted.FSKey AND deleted.ColumnNo = inserted.ColumnNo)

	UPDATE OutputSheetColumns
	SET tsModified = getdate()
	FROM dbo.OutputSheetColumns OutputSheetColumns
	INNER JOIN deleted d ON d.FSKey = OutputSheetColumns.FSKey AND d.ColumnNo = OutputSheetColumns.ColumnNo
	WHERE d.Refnum <> OutputSheetColumns.Refnum
	OR ((d.UnitID IS NULL AND OutputSheetColumns.UnitID IS NOT NULL) OR (d.UnitID IS NOT NULL AND OutputSheetColumns.UnitID IS NULL) OR d.UnitID <> OutputSheetColumns.UnitID)
	OR ((d.ColumnWidth IS NULL AND OutputSheetColumns.ColumnWidth IS NOT NULL) OR (d.ColumnWidth IS NOT NULL AND OutputSheetColumns.ColumnWidth IS NULL) OR d.ColumnWidth <> OutputSheetColumns.ColumnWidth)
	OR ((d.PGKey IS NULL AND OutputSheetColumns.PGKey IS NOT NULL) OR (d.PGKey IS NOT NULL AND OutputSheetColumns.PGKey IS NULL) OR d.PGKey <> OutputSheetColumns.PGKey)
	OR ((d.PPGKey IS NULL AND OutputSheetColumns.PPGKey IS NOT NULL) OR (d.PPGKey IS NOT NULL AND OutputSheetColumns.PPGKey IS NULL) OR d.PPGKey <> OutputSheetColumns.PPGKey)
	
	UPDATE OutputSheetColumns
	SET tsModified = getdate()
	FROM dbo.OutputSheetColumns OutputSheetColumns
	INNER JOIN #keychanged d ON d.FSKey = OutputSheetColumns.FSKey AND d.ColumnNo = OutputSheetColumns.ColumnNo
	
	DROP TABLE #keychanged
END

