﻿CREATE TABLE [dbo].[ProcessPeerGroupDef] (
    [PPGKey]      INT            IDENTITY (1000001, 1) NOT NULL,
    [PGKey]       INT            NOT NULL,
    [ProcessID]   VARCHAR (8)    NOT NULL,
    [ProcessType] VARCHAR (10)   CONSTRAINT [DF_ProcessPeerGroupDef_ProcessType] DEFAULT ('All') NOT NULL,
    [Class]       VARCHAR (50)   NULL,
    [SubClass]    VARCHAR (50)   NULL,
    [PeerType]    VARCHAR (50)   NULL,
    [RamGroup]    VARCHAR (50)   NULL,
    [LongText]    NVARCHAR (MAX) NOT NULL,
    [tsModified]  DATETIME       CONSTRAINT [DF_ProcessPeerGroupDef_tsModified] DEFAULT (getdate()) NOT NULL,
    [StudyRefnum] VARCHAR (25)   NOT NULL,
    [StudyUnitID] VARCHAR (25)   NOT NULL,
    CONSTRAINT [PK_ProcessPeerGroupDef] PRIMARY KEY CLUSTERED ([PPGKey] ASC),
    CONSTRAINT [FK_ProcessPeerGroupDef_PeerGroupDef] FOREIGN KEY ([PGKey]) REFERENCES [dbo].[PeerGroupDef] ([PGKey])
);


GO

CREATE TRIGGER dbo.SetProcessPeerGroupDefModifiedAfterUpdate
   ON  dbo.ProcessPeerGroupDef
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE ProcessPeerGroupDef
	SET tsModified = getdate()
	FROM dbo.ProcessPeerGroupDef ProcessPeerGroupDef
	INNER JOIN deleted d ON d.PPGKey = ProcessPeerGroupDef.PPGKey
	WHERE d.ProcessID <> ProcessPeerGroupDef.ProcessID
	OR d.ProcessType <> ProcessPeerGroupDef.ProcessType
	OR d.Class <> ProcessPeerGroupDef.Class OR (d.Class IS NULL AND ProcessPeerGroupDef.Class IS NOT NULL) OR (d.Class IS NOT NULL AND ProcessPeerGroupDef.Class IS NULL)
	OR d.SubClass <> ProcessPeerGroupDef.SubClass OR (d.SubClass IS NULL AND ProcessPeerGroupDef.SubClass IS NOT NULL) OR (d.SubClass IS NOT NULL AND ProcessPeerGroupDef.SubClass IS NULL)
	OR d.PeerType <> ProcessPeerGroupDef.PeerType OR (d.PeerType IS NULL AND ProcessPeerGroupDef.PeerType IS NOT NULL) OR (d.PeerType IS NOT NULL AND ProcessPeerGroupDef.PeerType IS NULL)
	OR d.RamGroup <> ProcessPeerGroupDef.RamGroup OR (d.RamGroup IS NULL AND ProcessPeerGroupDef.RamGroup IS NOT NULL) OR (d.RamGroup IS NOT NULL AND ProcessPeerGroupDef.RamGroup IS NULL)
	OR d.LongText <> ProcessPeerGroupDef.LongText
	OR d.StudyRefnum <> ProcessPeerGroupDef.StudyRefnum
	OR d.StudyUnitID <> ProcessPeerGroupDef.StudyUnitID
	
END

GO

CREATE TRIGGER dbo.SetProcessPeerGroupDefModifiedAfterInsert
   ON  dbo.ProcessPeerGroupDef
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE ProcessPeerGroupDef
	SET tsModified = getdate()
	FROM dbo.ProcessPeerGroupDef ProcessPeerGroupDef
	INNER JOIN inserted i ON i.PPGKey = ProcessPeerGroupDef.PPGKey
	
END
