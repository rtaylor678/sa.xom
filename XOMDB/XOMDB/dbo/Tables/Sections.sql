﻿CREATE TABLE [dbo].[Sections] (
    [SectionCode] VARCHAR (255) NOT NULL,
    [SectionName] VARCHAR (255) NOT NULL,
    [tsModified]  DATETIME      CONSTRAINT [DF_Sections_tsModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Sections] PRIMARY KEY CLUSTERED ([SectionCode] ASC)
);


GO

CREATE TRIGGER [dbo].[SetSectionsModifiedAfterInsert]
   ON  [dbo].[Sections]
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE Sections
	SET tsModified = getdate()
	FROM dbo.Sections Sections
	INNER JOIN inserted i ON i.SectionCode = Sections.SectionCode
	
END

GO

CREATE TRIGGER [dbo].[SetSectionsModifiedAfterUpdate]
   ON  [dbo].[Sections]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE Sections
	SET tsModified = getdate()
	FROM dbo.Sections Sections
	INNER JOIN deleted d ON d.SectionCode = Sections.SectionCode
	WHERE	d.SectionName <> Sections.SectionName
	
END
