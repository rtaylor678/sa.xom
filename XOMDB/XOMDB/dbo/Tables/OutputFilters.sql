﻿CREATE TABLE [dbo].[OutputFilters] (
    [MasterFileFilter]         VARCHAR (MAX) NOT NULL,
    [FilterNoCurrencyScenario] VARCHAR (MAX) NULL,
    [tsModified]               SMALLDATETIME DEFAULT (getdate()) NOT NULL
);

