﻿CREATE TABLE [dbo].[OutputFileSections] (
    [Study]       VARCHAR (10)  NULL,
    [SectionCode] VARCHAR (255) NULL,
    [tsModified]  SMALLDATETIME DEFAULT (getdate()) NOT NULL
);


GO

CREATE TRIGGER [dbo].[SetOutputFileSectionsModifiedAfterInsert]
   ON  [dbo].[OutputFileSections]
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE OutputFileSections
	SET tsModified = getdate()
	FROM dbo.OutputFileSections OutputFileSections
	INNER JOIN inserted i ON i.Study = OutputFileSections.Study AND i.SectionCode = OutputFileSections.SectionCode
	
END


GO

CREATE TRIGGER [dbo].[SetOutputFileSectionsModifiedAfterUpdate]
   ON  [dbo].[OutputFileSections]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE OutputFileSections
	SET tsModified = getdate()
	FROM dbo.OutputFileSections OutputFileSections
	INNER JOIN inserted d ON d.Study = OutputFileSections.Study AND d.SectionCode = OutputFileSections.SectionCode
	
END

