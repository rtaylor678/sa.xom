﻿CREATE TABLE [dbo].[PeerGroupHierarchyMajorGroups] (
    [MajorGroup]     VARCHAR (15)  NOT NULL,
    [MajorGroupDesc] VARCHAR (100) NOT NULL,
    [tsModified]     DATETIME      DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_PeerGroupHierarchyMajorGroups] PRIMARY KEY CLUSTERED ([MajorGroup] ASC)
);


GO

CREATE TRIGGER [dbo].[SetPeerGroupHierarchyMajorGroupsModifiedAfterInsert]
   ON  [dbo].[PeerGroupHierarchyMajorGroups]
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE PeerGroupHierarchyMajorGroups
	SET tsModified = getdate()
	FROM dbo.PeerGroupHierarchyMajorGroups PeerGroupHierarchyMajorGroups
	INNER JOIN inserted i ON i.MajorGroup = PeerGroupHierarchyMajorGroups.MajorGroup
	
END


GO

CREATE TRIGGER [dbo].[SetPeerGroupHierarchyMajorGroupsModifiedAfterUpdate]
   ON  [dbo].[PeerGroupHierarchyMajorGroups]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	IF UPDATE (MajorGroup) OR UPDATE (MajorGroupDesc)
		UPDATE PeerGroupHierarchyMajorGroups
		SET tsModified =GETDATE()
		FROM dbo.PeerGroupHierarchyMajorGroups PeerGroupHierarchyMajorGroups 
		INNER JOIN deleted d ON d.MajorGroup = PeerGroupHierarchyMajorGroups.MajorGroup
	
END

