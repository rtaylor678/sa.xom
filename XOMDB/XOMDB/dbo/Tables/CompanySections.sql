﻿CREATE TABLE [dbo].[CompanySections] (
    [CompanyKey]  SMALLINT      NOT NULL,
    [SectionCode] VARCHAR (255) NOT NULL,
    [tsAdded]     DATETIME      CONSTRAINT [DF_CompanySections_tsAdded] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_CompanySections] PRIMARY KEY CLUSTERED ([CompanyKey] ASC, [SectionCode] ASC)
);

