﻿CREATE TABLE [dbo].[ReportTypes] (
    [ReportType] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ReportTypes] PRIMARY KEY CLUSTERED ([ReportType] ASC)
);

