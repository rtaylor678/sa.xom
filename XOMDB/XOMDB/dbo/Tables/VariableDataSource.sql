﻿CREATE TABLE [dbo].[VariableDataSource] (
    [Variable]   VARCHAR (255) NOT NULL,
    [TableName]  VARCHAR (255) NOT NULL,
    [ColumnName] VARCHAR (255) NOT NULL,
    [Filter]     VARCHAR (255) NULL,
    [StudyDB]    VARCHAR (255) NULL
);

