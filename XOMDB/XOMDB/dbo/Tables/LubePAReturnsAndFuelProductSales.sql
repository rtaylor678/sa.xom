﻿CREATE TABLE [dbo].[LubePAReturnsAndFuelProductSales] (
    [PGKey]                INT            NOT NULL,
    [MaterialKey]          INT            NOT NULL,
    [MaterialID]           VARCHAR (5)    NOT NULL,
    [MaterialName]         NVARCHAR (50)  NOT NULL,
    [Bbl]                  INT            NULL,
    [PlantProdImpact]      NUMERIC (6, 2) NULL,
    [FinProdImpact]        NUMERIC (6, 2) NULL,
    [CashMarginBblImpact]  NUMERIC (6, 2) NULL,
    [CashMarginImpactMUSD] NUMERIC (6, 2) NULL,
    [tsModified]           DATETIME       CONSTRAINT [DF_LubePAReturnsAndFuelProductSales_tsModified] DEFAULT (getdate()) NOT NULL,
    [tsVerified]           DATETIME       CONSTRAINT [DF_LubePAReturnsAndFuelProductSales_tsVerified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_LubePAReturnsAndFuelProductSales] PRIMARY KEY CLUSTERED ([PGKey] ASC, [MaterialKey] ASC)
);

