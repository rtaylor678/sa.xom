﻿CREATE TABLE [dbo].[ProcessVsPeer] (
    [ProcessPeerKey] INT         NOT NULL,
    [VKey]           INT         NOT NULL,
    [Currency]       VARCHAR (4) DEFAULT ('USD') NOT NULL,
    [numberValue]    REAL        NULL,
    [tsModified]     DATETIME    CONSTRAINT [DF_ProcessOpps_tsModified] DEFAULT (getdate()) NOT NULL,
    [tsVerified]     DATETIME    CONSTRAINT [DF_ProcessOpps_tsVerified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_ProcessOpps] PRIMARY KEY CLUSTERED ([ProcessPeerKey] ASC, [VKey] ASC, [Currency] ASC),
    CONSTRAINT [FK_ProcessOppsPeerKey_ProcessPeerKey] FOREIGN KEY ([ProcessPeerKey]) REFERENCES [dbo].[ProcessPeers] ([ProcessPeerKey]),
    CONSTRAINT [FK_ProcessOppsVKey_VarDefVKey] FOREIGN KEY ([VKey]) REFERENCES [dbo].[VarDef] ([VKey])
);


GO



CREATE TRIGGER [dbo].[SetProcessVsPeerModifiedAfterUpdate]
   ON  [dbo].[ProcessVsPeer]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	IF UPDATE (ProcessPeerKey) OR UPDATE (VKey) OR UPDATE(Currency) OR UPDATE(numberValue)
		UPDATE ProcessVsPeer
		SET tsVerified = getdate()
		, tsModified = CASE 
			WHEN (d.numberValue IS NULL AND ProcessVsPeer.numberValue IS NOT NULL) OR (d.numberValue IS NOT NULL AND ProcessVsPeer.numberValue IS NULL) THEN GETDATE()
			WHEN (d.numberValue IS NOT NULL AND ProcessVsPeer.numberValue IS NOT NULL) AND ROUND(d.numberValue, v.Decimals+2) <> ROUND(ProcessVsPeer.numberValue, v.Decimals+2) THEN GETDATE()
			ELSE d.tsModified END
		FROM dbo.ProcessVsPeer ProcessVsPeer INNER JOIN dbo.VarDef v ON v.VKey = ProcessVsPeer.VKey
		INNER JOIN deleted d ON d.ProcessPeerKey = ProcessVsPeer.ProcessPeerKey AND d.VKey = ProcessVsPeer.VKey AND d.Currency = ProcessVsPeer.Currency
	
END



GO



CREATE TRIGGER [dbo].[SetProcessVsPeerModifiedAfterInsert]
   ON  [dbo].[ProcessVsPeer]
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE ProcessVsPeer
	SET tsModified = getdate(), tsVerified = GETDATE()
	FROM dbo.ProcessVsPeer ProcessVsPeer
	INNER JOIN inserted i ON i.ProcessPeerKey = ProcessVsPeer.ProcessPeerKey
	
END


