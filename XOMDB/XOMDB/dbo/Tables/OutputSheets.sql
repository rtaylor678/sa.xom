﻿CREATE TABLE [dbo].[OutputSheets] (
    [FSKey]      INT           IDENTITY (1, 1) NOT NULL,
    [FKey]       INT           NOT NULL,
    [SheetNo]    SMALLINT      NOT NULL,
    [SheetName]  VARCHAR (50)  NOT NULL,
    [tsModified] SMALLDATETIME DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_OutputSheets] PRIMARY KEY CLUSTERED ([FSKey] ASC),
    CONSTRAINT [FK_OutputSheets_OutputFiles] FOREIGN KEY ([FKey]) REFERENCES [dbo].[OutputFiles] ([FKey])
);


GO
CREATE NONCLUSTERED INDEX [IX_OutputSheets_SheetName]
    ON [dbo].[OutputSheets]([FKey] ASC, [SheetName] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_OutputSheets_SheetNo]
    ON [dbo].[OutputSheets]([FKey] ASC, [SheetNo] ASC);


GO

CREATE TRIGGER [dbo].[SetOutputSheetsModifiedAfterUpdate]
   ON  [dbo].[OutputSheets]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	SELECT FSKey INTO #keychanged 
	FROM inserted 
	WHERE NOT EXISTS (SELECT * FROM deleted WHERE deleted.FSKey = inserted.FSKey)

	UPDATE OutputSheets
	SET tsModified = getdate()
	FROM dbo.OutputSheets OutputSheets
	INNER JOIN deleted d ON d.FSKey = OutputSheets.FSKey
	WHERE d.FKey <> OutputSheets.FKey
	OR d.SheetNo <> OutputSheets.SheetNo
	OR d.SheetName <> OutputSheets.SheetName
	
	UPDATE OutputSheets
	SET tsModified = getdate()
	FROM dbo.OutputSheets OutputSheets
	INNER JOIN #keychanged d ON d.FSKey = OutputSheets.FSKey
	
	DROP TABLE #keychanged
END


GO

CREATE TRIGGER [dbo].[SeOutputSheetsModifiedAfterInsert]
   ON  [dbo].[OutputSheets]
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE OutputSheets
	SET tsModified = getdate()
	FROM dbo.OutputSheets OutputSheets
	INNER JOIN inserted i ON i.FSKey = OutputSheets.FSKey
	
END

