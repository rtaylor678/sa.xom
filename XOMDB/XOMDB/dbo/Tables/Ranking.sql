﻿CREATE TABLE [dbo].[Ranking] (
    [FacilityPGKey]  INT      NOT NULL,
    [PeerGroupPGKey] INT      NOT NULL,
    [VKey]           INT      NOT NULL,
    [Value]          REAL     NULL,
    [DataCount]      REAL     NULL,
    [Tile]           TINYINT  NULL,
    [PercentRanking] REAL     NULL,
    [tsModified]     DATETIME CONSTRAINT [DF_Ranking_tsModified] DEFAULT (getdate()) NOT NULL,
    [tsVerified]     DATETIME CONSTRAINT [DF_Ranking_tsVerified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Ranking] PRIMARY KEY CLUSTERED ([FacilityPGKey] ASC, [PeerGroupPGKey] ASC, [VKey] ASC)
);


GO

CREATE TRIGGER dbo.SetRankingModifiedAfterUpdate
   ON  dbo.Ranking
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE Ranking
	SET tsVerified = getdate(), tsModified = CASE WHEN chk.Changed = 1 THEN getdate() ELSE d.tsModified END
	FROM dbo.Ranking Ranking
	INNER JOIN deleted d ON d.FacilityPGKey = Ranking.FacilityPGKey AND d.PeerGroupPGKey = Ranking.PeerGroupPGKey AND d.VKey = Ranking.VKey
	CROSS APPLY (VALUES (CASE WHEN 
			d.Value <> Ranking.Value OR (d.Value IS NULL AND Ranking.Value IS NOT NULL) OR (d.Value IS NOT NULL AND Ranking.Value IS NULL)
			OR d.DataCount <> Ranking.DataCount OR (d.DataCount IS NULL AND Ranking.DataCount IS NOT NULL) OR (d.DataCount IS NOT NULL AND Ranking.DataCount IS NULL)
			OR d.Tile <> Ranking.Tile OR (d.Tile IS NULL AND Ranking.Tile IS NOT NULL) OR (d.Tile IS NOT NULL AND Ranking.Tile IS NULL)
			OR d.PercentRanking <> Ranking.PercentRanking OR (d.PercentRanking IS NULL AND Ranking.PercentRanking IS NOT NULL) OR (d.PercentRanking IS NOT NULL AND Ranking.PercentRanking IS NULL) 
		THEN 1 ELSE 0 END)) chk(Changed)
	
END

GO

CREATE TRIGGER dbo.SetRankingModifiedAfterInsert
   ON  dbo.Ranking
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE Ranking
	SET tsModified = getdate(), tsVerified = getdate()
	FROM dbo.Ranking Ranking
	INNER JOIN inserted i ON i.FacilityPGKey = Ranking.FacilityPGKey AND i.PeerGroupPGKey = Ranking.PeerGroupPGKey AND i.VKey = Ranking.VKey
	
END
