﻿CREATE TABLE [dbo].[VarHierarchyList] (
    [VHKey]      INT           IDENTITY (1, 1) NOT NULL,
    [VHName]     VARCHAR (100) NOT NULL,
    [VHRootVKey] INT           NOT NULL,
    [tsModified] DATETIME      CONSTRAINT [DF_VarHierarchyList_tsModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_VarHierarchyList] PRIMARY KEY CLUSTERED ([VHKey] ASC),
    CONSTRAINT [FK_VarHierarchyList_VarDef] FOREIGN KEY ([VHRootVKey]) REFERENCES [dbo].[VarDef] ([VKey]),
    CONSTRAINT [IX_VarHierarchyList] UNIQUE NONCLUSTERED ([VHName] ASC)
);


GO

CREATE TRIGGER dbo.SetVarHierarchyListModifiedAfterInsert
   ON  dbo.VarHierarchyList
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE VarHierarchyList
	SET tsModified = getdate()
	FROM dbo.VarHierarchyList VarHierarchyList
	INNER JOIN inserted i ON i.VHKey = VarHierarchyList.VHKey
	
END

GO

CREATE TRIGGER dbo.SetVarHierarchyListModifiedAfterUpdate
   ON  dbo.VarHierarchyList
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE VarHierarchyList
	SET tsModified = getdate()
	FROM dbo.VarHierarchyList VarHierarchyList
	INNER JOIN deleted d ON d.VHKey = VarHierarchyList.VHKey
	WHERE	d.VHName <> VarHierarchyList.VHName OR d.VHRootVKey <> VarHierarchyList.VHRootVKey
	
END
