﻿CREATE TABLE [dbo].[OutputSheetRows] (
    [FSKey]              INT            NOT NULL,
    [RowNo]              INT            NOT NULL,
    [RowText]            NVARCHAR (255) NULL,
    [TextIndent]         TINYINT        CONSTRAINT [DF_OutputSheetRows_TextIndent] DEFAULT ((0)) NULL,
    [Bold]               BIT            CONSTRAINT [DF_OutputSheetRows_Bold] DEFAULT ((0)) NULL,
    [NumberFormat]       VARCHAR (255)  NULL,
    [DBTableName]        VARCHAR (255)  NULL,
    [DBColumnName]       VARCHAR (255)  NULL,
    [DBFilter]           VARCHAR (MAX)  NULL,
    [SectionCode]        VARCHAR (255)  NULL,
    [MasterFileVariable] VARCHAR (255)  NULL,
    [Hidden]             BIT            CONSTRAINT [DF_OutputSheetRows_Hidden] DEFAULT ((0)) NOT NULL,
    [RowHeight]          REAL           NULL,
    [VKey]               INT            NULL,
    [tsModified]         SMALLDATETIME  DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_OutputSheetRows] PRIMARY KEY CLUSTERED ([FSKey] ASC, [RowNo] ASC)
);


GO

CREATE TRIGGER [dbo].[SetOutputSheetRowsModifiedAfterInsert]
   ON  [dbo].[OutputSheetRows]
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE OutputSheetRows
	SET tsModified = getdate()
	FROM dbo.OutputSheetRows OutputSheetRows
	INNER JOIN inserted i ON i.FSKey = OutputSheetRows.FSKey AND i.RowNo = OutputSheetRows.RowNo
	
END


GO

CREATE TRIGGER [dbo].[SetOutputSheetRowsModifiedAfterUpdate]
   ON  [dbo].[OutputSheetRows]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	SELECT FSKey, RowNo INTO #keychanged 
	FROM inserted 
	WHERE NOT EXISTS (SELECT * FROM deleted WHERE deleted.FSKey = inserted.FSKey AND deleted.RowNo = inserted.RowNo)

	UPDATE OutputSheetRows
	SET tsModified = getdate()
	FROM dbo.OutputSheetRows OutputSheetRows
	INNER JOIN deleted d ON d.FSKey = OutputSheetRows.FSKey AND d.RowNo = OutputSheetRows.RowNo
	WHERE ((d.RowText IS NULL AND OutputSheetRows.RowText IS NOT NULL) OR (d.RowText IS NOT NULL AND OutputSheetRows.RowText IS NULL) OR d.RowText <> OutputSheetRows.RowText)
	OR ((d.TextIndent IS NULL AND OutputSheetRows.TextIndent IS NOT NULL) OR (d.TextIndent IS NOT NULL AND OutputSheetRows.TextIndent IS NULL) OR d.TextIndent <> OutputSheetRows.TextIndent)
	OR ((d.Bold IS NULL AND OutputSheetRows.Bold IS NOT NULL) OR (d.Bold IS NOT NULL AND OutputSheetRows.Bold IS NULL) OR d.Bold <> OutputSheetRows.Bold)
	OR ((d.NumberFormat IS NULL AND OutputSheetRows.NumberFormat IS NOT NULL) OR (d.NumberFormat IS NOT NULL AND OutputSheetRows.NumberFormat IS NULL) OR d.NumberFormat <> OutputSheetRows.NumberFormat)
	OR ((d.DBTableName IS NULL AND OutputSheetRows.DBTableName IS NOT NULL) OR (d.DBTableName IS NOT NULL AND OutputSheetRows.DBTableName IS NULL) OR d.DBTableName <> OutputSheetRows.DBTableName)
	OR ((d.DBColumnName IS NULL AND OutputSheetRows.DBColumnName IS NOT NULL) OR (d.DBColumnName IS NOT NULL AND OutputSheetRows.DBColumnName IS NULL) OR d.DBColumnName <> OutputSheetRows.DBColumnName)
	OR ((d.DBFilter IS NULL AND OutputSheetRows.DBFilter IS NOT NULL) OR (d.DBFilter IS NOT NULL AND OutputSheetRows.DBFilter IS NULL) OR d.DBFilter <> OutputSheetRows.DBFilter)
	OR ((d.SectionCode IS NULL AND OutputSheetRows.SectionCode IS NOT NULL) OR (d.SectionCode IS NOT NULL AND OutputSheetRows.SectionCode IS NULL) OR d.SectionCode <> OutputSheetRows.SectionCode)
	OR ((d.MasterFileVariable IS NULL AND OutputSheetRows.MasterFileVariable IS NOT NULL) OR (d.MasterFileVariable IS NOT NULL AND OutputSheetRows.MasterFileVariable IS NULL) OR d.MasterFileVariable <> OutputSheetRows.MasterFileVariable)
	OR ((d.Hidden IS NULL AND OutputSheetRows.Hidden IS NOT NULL) OR (d.Hidden IS NOT NULL AND OutputSheetRows.Hidden IS NULL) OR d.Hidden <> OutputSheetRows.Hidden)
	OR ((d.RowHeight IS NULL AND OutputSheetRows.RowHeight IS NOT NULL) OR (d.RowHeight IS NOT NULL AND OutputSheetRows.RowHeight IS NULL) OR d.RowHeight <> OutputSheetRows.RowHeight)
	OR ((d.VKey IS NULL AND OutputSheetRows.VKey IS NOT NULL) OR (d.VKey IS NOT NULL AND OutputSheetRows.VKey IS NULL) OR d.VKey <> OutputSheetRows.VKey)
	
	UPDATE OutputSheetRows
	SET tsModified = getdate()
	FROM dbo.OutputSheetRows OutputSheetRows
	INNER JOIN #keychanged d ON d.FSKey = OutputSheetRows.FSKey AND d.RowNo = OutputSheetRows.RowNo
	
	DROP TABLE #keychanged
END

