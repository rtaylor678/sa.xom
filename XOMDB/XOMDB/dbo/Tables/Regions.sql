﻿CREATE TABLE [dbo].[Regions] (
    [RegionCode] VARCHAR (5)  NOT NULL,
    [Region]     VARCHAR (50) NOT NULL,
    [tsModified] DATETIME     CONSTRAINT [DF__Regions__tsModif__1B9317B3] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Regions] PRIMARY KEY CLUSTERED ([RegionCode] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Regions_Region]
    ON [dbo].[Regions]([Region] ASC);


GO
CREATE TRIGGER [dbo].[SetRegionsModifiedAfterInsert]
   ON  dbo.Regions
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE Regions
	SET tsModified = getdate()
	FROM dbo.Regions Regions
	INNER JOIN inserted i ON i.Region = Regions.Region
	
END

GO
Create TRIGGER [dbo].[SetRegionsModifiedAfterUpdate]
   ON  dbo.Regions
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE Regions
	SET tsModified = getdate()
	FROM dbo.Regions Regions
	INNER JOIN deleted d ON (d.Region = Regions.Region OR d.RegionCode = Regions.RegionCode)
	WHERE d.Region <> Regions.Region OR d.RegionCode <> Regions.RegionCode
		
END
