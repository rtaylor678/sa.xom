﻿CREATE TABLE [dbo].[StudyYears] (
    [StudyCode]   VARCHAR (10) NOT NULL,
    [StudyYear]   SMALLINT     NOT NULL,
    [StudyYearID] INT          IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [PK_StudyYears] PRIMARY KEY NONCLUSTERED ([StudyYearID] ASC),
    CONSTRAINT [FK_StudyYears_Studies] FOREIGN KEY ([StudyCode]) REFERENCES [dbo].[Studies] ([StudyCode])
);


GO
CREATE UNIQUE CLUSTERED INDEX [IX_StudyYears_CodeYear]
    ON [dbo].[StudyYears]([StudyCode] ASC, [StudyYear] ASC);

