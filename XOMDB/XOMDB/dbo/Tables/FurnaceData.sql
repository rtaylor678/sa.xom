﻿CREATE TABLE [dbo].[FurnaceData] (
    [FPGKey]      INT            NOT NULL,
    [VKey]        INT            NOT NULL,
    [numberValue] FLOAT (53)     NULL,
    [textValue]   NVARCHAR (MAX) NULL,
    [tsModified]  DATETIME       CONSTRAINT [DF_FurnaceData_tsModified] DEFAULT (getdate()) NOT NULL,
    [tsVerified]  DATETIME       CONSTRAINT [DF_FurnaceData_tsVerified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_FurnaceData] PRIMARY KEY CLUSTERED ([FPGKey] ASC, [VKey] ASC),
    CONSTRAINT [FK_FurnaceData_FurnacePeerGroupDef] FOREIGN KEY ([FPGKey]) REFERENCES [dbo].[FurnacePeerGroupDef] ([FPGKey]),
    CONSTRAINT [FK_FurnaceData_VarDef] FOREIGN KEY ([VKey]) REFERENCES [dbo].[VarDef] ([VKey])
);


GO


CREATE TRIGGER [dbo].[SetFurnaceDataModifiedAfterInsert]
   ON  [dbo].[FurnaceData]
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE FurnaceData
	SET tsModified = getdate(), tsVerified = getdate()
	FROM dbo.FurnaceData FurnaceData
	INNER JOIN inserted i ON i.FPGKey = FurnaceData.FPGKey AND i.VKey = FurnaceData.VKey
	
END


GO


CREATE TRIGGER [dbo].[SetFurnaceDataModifiedAfterUpdate]
   ON  [dbo].[FurnaceData]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	IF UPDATE (FPGKey) OR UPDATE (VKey) OR UPDATE(numberValue) OR UPDATE(textValue)
		UPDATE FurnaceData
		SET tsVerified = getdate()
		, tsModified = CASE 
			WHEN (d.numberValue IS NULL AND FurnaceData.numberValue IS NOT NULL) OR (d.numberValue IS NOT NULL AND FurnaceData.numberValue IS NULL) THEN GETDATE()
			WHEN (d.numberValue IS NOT NULL AND FurnaceData.numberValue IS NOT NULL) AND ROUND(d.numberValue, v.Decimals+2) <> ROUND(FurnaceData.numberValue, v.Decimals+2) THEN GETDATE()
			WHEN (d.textValue IS NULL AND FurnaceData.textValue IS NOT NULL) OR (d.textValue IS NOT NULL AND FurnaceData.textValue IS NULL) THEN GETDATE()
			WHEN (d.textValue IS NOT NULL AND FurnaceData.textValue IS NOT NULL) AND d.textValue <> FurnaceData.textValue THEN GETDATE()
			ELSE d.tsModified END
		FROM dbo.FurnaceData FurnaceData INNER JOIN dbo.VarDef v ON v.VKey = FurnaceData.VKey
		INNER JOIN deleted d ON d.FPGKey = FurnaceData.FPGKey AND d.VKey = FurnaceData.VKey
	
END

