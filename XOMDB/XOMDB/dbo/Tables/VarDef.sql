﻿CREATE TABLE [dbo].[VarDef] (
    [VKey]               INT            IDENTITY (1, 1) NOT NULL,
    [Variable]           VARCHAR (255)  NOT NULL,
    [UOM]                VARCHAR (50)   NULL,
    [LongText]           NVARCHAR (MAX) NOT NULL,
    [DataType]           VARCHAR (10)   NOT NULL,
    [Decimals]           TINYINT        NOT NULL,
    [tsModified]         DATETIME       CONSTRAINT [DF_VarDef_tsModified] DEFAULT (getdate()) NOT NULL,
    [MasterFileVariable] VARCHAR (255)  NULL,
    [IsMetric]           BIT            CONSTRAINT [DF_VarDef_IsMetric] DEFAULT ((0)) NOT NULL,
    [SectionCode]        VARCHAR (255)  NULL,
    [InOutput]           BIT            DEFAULT ((0)) NOT NULL,
    [InOpexPA]           BIT            DEFAULT ((0)) NOT NULL,
    [InMarginPA]         BIT            DEFAULT ((0)) NOT NULL,
    [InEnergyPA]         BIT            DEFAULT ((0)) NOT NULL,
    [InCEIPA]            BIT            DEFAULT ((0)) NOT NULL,
    [InUnitYield]        BIT            DEFAULT ((0)) NOT NULL,
    [InRAMTable]         BIT            DEFAULT ((0)) NOT NULL,
    [InProcess]          BIT            DEFAULT ((0)) NOT NULL,
    [InLubesPA]          BIT            DEFAULT ((0)) NOT NULL,
    [InFurnaceData]      BIT            DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_VarDef] PRIMARY KEY CLUSTERED ([VKey] ASC),
    CONSTRAINT [FK_VarDef_Sections] FOREIGN KEY ([SectionCode]) REFERENCES [dbo].[Sections] ([SectionCode])
);


GO
CREATE NONCLUSTERED INDEX [IX_VarDef_UniqueVariable]
    ON [dbo].[VarDef]([Variable] ASC, [UOM] ASC);


GO

CREATE TRIGGER dbo.SetVarDefModifiedAfterInsert
   ON  dbo.VarDef
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE VarDef
	SET tsModified = getdate()
	FROM dbo.VarDef VarDef
	INNER JOIN inserted i ON i.VKey = VarDef.VKey
	
END

GO

CREATE TRIGGER dbo.SetVarDefModifiedAfterUpdate
   ON  dbo.VarDef
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE VarDef
	SET tsModified = getdate()
	FROM dbo.VarDef VarDef
	INNER JOIN deleted d ON d.VKey = VarDef.VKey
	WHERE	d.Variable <> VarDef.Variable OR d.LongText <> VarDef.LongText OR d.DataType <> VarDef.DataType OR d.Decimals <> VarDef.Decimals OR d.IsMetric <> VarDef.IsMetric
			OR d.UOM <> VarDef.UOM OR (d.UOM IS NULL AND VarDef.UOM IS NOT NULL) OR (d.UOM IS NOT NULL AND VarDef.UOM IS NULL)
			OR d.SectionCode <> VarDef.SectionCode OR (d.SectionCode IS NULL AND VarDef.SectionCode IS NOT NULL) OR (d.SectionCode IS NOT NULL AND VarDef.SectionCode IS NULL)
	
END
