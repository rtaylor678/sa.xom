﻿CREATE TABLE [dbo].[VarHierarchyItems] (
    [VHKey]         INT      NOT NULL,
    [ItemVKey]      INT      NOT NULL,
    [ParentVKey]    INT      NOT NULL,
    [ItemSortOrder] INT      CONSTRAINT [DF_VarHierarchyItems_ItemSortOrder] DEFAULT ((1)) NOT NULL,
    [tsModified]    DATETIME CONSTRAINT [DF_VarHierarchyItems_tsModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_VarHierarchyItems] PRIMARY KEY CLUSTERED ([VHKey] ASC, [ItemVKey] ASC),
    CONSTRAINT [FK_VarHierarchyItems_VarDef] FOREIGN KEY ([ItemVKey]) REFERENCES [dbo].[VarDef] ([VKey]),
    CONSTRAINT [FK_VarHierarchyItems_VarHierarchyList] FOREIGN KEY ([VHKey]) REFERENCES [dbo].[VarHierarchyList] ([VHKey])
);


GO

CREATE TRIGGER dbo.SetVarHierarchyItemsModifiedAfterUpdate
   ON  dbo.VarHierarchyItems
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE VarHierarchyItems
	SET tsModified = getdate()
	FROM dbo.VarHierarchyItems VarHierarchyItems
	INNER JOIN deleted d ON d.VHKey = VarHierarchyItems.VHKey AND d.ItemVKey = VarHierarchyItems.ItemVKey
	WHERE	d.ParentVKey <> VarHierarchyItems.ParentVKey OR d.ItemSortOrder <> VarHierarchyItems.ItemSortOrder
	
END

GO

CREATE TRIGGER dbo.SetVarHierarchyItemsModifiedAfterInsert
   ON  dbo.VarHierarchyItems
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE VarHierarchyItems
	SET tsModified = getdate()
	FROM dbo.VarHierarchyItems VarHierarchyItems
	INNER JOIN inserted i ON i.VHKey = VarHierarchyItems.VHKey AND i.ItemVKey = VarHierarchyItems.ItemVKey
	
END
