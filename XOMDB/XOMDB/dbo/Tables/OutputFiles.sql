﻿CREATE TABLE [dbo].[OutputFiles] (
    [Filename]   VARCHAR (255) NOT NULL,
    [StudyYear]  SMALLINT      NULL,
    [Study]      VARCHAR (10)  NULL,
    [FileType]   VARCHAR (255) NULL,
    [FKey]       INT           IDENTITY (1, 1) NOT NULL,
    [Currency]   VARCHAR (4)   CONSTRAINT [DF_OutputFiles_Currency] DEFAULT ('USD') NULL,
    [tsModified] DATETIME      CONSTRAINT [DF_OutputFiles_tsModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_OutputFiles] PRIMARY KEY CLUSTERED ([FKey] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_OutputFiles]
    ON [dbo].[OutputFiles]([Filename] ASC);


GO

CREATE TRIGGER [dbo].[SetOutputFilesModifiedAfterInsert]
   ON  [dbo].[OutputFiles]
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE OutputFiles
	SET tsModified = getdate()
	FROM dbo.OutputFiles OutputFiles
	INNER JOIN inserted i ON i.FKey = OutputFiles.FKey
	
END


GO

CREATE TRIGGER [dbo].[SetOutputFilesModifiedAfterUpdate]
   ON  [dbo].[OutputFiles]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE OutputFiles
	SET tsModified = getdate()
	FROM dbo.OutputFiles OutputFiles
	INNER JOIN deleted d ON d.FKey = OutputFiles.FKey
	WHERE d.Filename <> OutputFiles.Filename
	OR d.StudyYear <> OutputFiles.StudyYear
	OR d.Study <> OutputFiles.Study
	OR d.FileType <> OutputFiles.FileType
	OR d.Currency <> OutputFiles.Currency
	
END

