﻿CREATE TABLE [dbo].[CompanyPPGKeys] (
    [CompanyKey] SMALLINT NOT NULL,
    [PPGKey]     INT      NOT NULL,
    [tsAdded]    DATETIME CONSTRAINT [DF_CompanyPPGKeys_tsAdded] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_CompanyPPGKeys] PRIMARY KEY CLUSTERED ([CompanyKey] ASC, [PPGKey] ASC)
);

