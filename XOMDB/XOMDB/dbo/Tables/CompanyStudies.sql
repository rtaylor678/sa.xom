﻿CREATE TABLE [dbo].[CompanyStudies] (
    [CompanyID]  VARCHAR (10) NOT NULL,
    [Study]      VARCHAR (10) NOT NULL,
    [StudyYear]  SMALLINT     NOT NULL,
    [tsModified] DATETIME     CONSTRAINT [DF_CompanyStudies_tsModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_CompanyStudies] PRIMARY KEY CLUSTERED ([CompanyID] ASC, [Study] ASC, [StudyYear] ASC)
);

