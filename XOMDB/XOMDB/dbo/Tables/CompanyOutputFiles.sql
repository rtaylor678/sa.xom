﻿CREATE TABLE [dbo].[CompanyOutputFiles] (
    [CompanyID]  VARCHAR (10) NOT NULL,
    [FKey]       INT          NOT NULL,
    [tsModified] DATETIME     CONSTRAINT [DF_CompanyOutputFiles_tsModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_CompanyOutputFiles] PRIMARY KEY CLUSTERED ([CompanyID] ASC, [FKey] ASC)
);

