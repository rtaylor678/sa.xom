﻿CREATE TABLE [dbo].[FurnacePeerGroupDef] (
    [FPGKey]         INT            IDENTITY (2000001, 1) NOT NULL,
    [PPGKey]         INT            NOT NULL,
    [FurnaceService] VARCHAR (50)   NULL,
    [LongText]       NVARCHAR (MAX) NOT NULL,
    [tsModified]     DATETIME       CONSTRAINT [DF_FurnacePeerGroupDef_tsModified] DEFAULT (getdate()) NOT NULL,
    [StudyRefnum]    VARCHAR (25)   NOT NULL,
    [StudyUnitID]    VARCHAR (25)   NOT NULL,
    [StudyService]   VARCHAR (25)   NULL,
    [HeaterNo]       SMALLINT       NULL,
    CONSTRAINT [PK_FurnacePeerGroupDef] PRIMARY KEY CLUSTERED ([FPGKey] ASC),
    CONSTRAINT [FK_FurnacePeerGroupDef_ProcessPeerGroupDef] FOREIGN KEY ([PPGKey]) REFERENCES [dbo].[ProcessPeerGroupDef] ([PPGKey])
);


GO
CREATE TRIGGER [dbo].[SetFurnacePeerGroupDefModifiedAfterInsert]
   ON  dbo.FurnacePeerGroupDef
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE FurnacePeerGroupDef
	SET tsModified = getdate()
	FROM dbo.FurnacePeerGroupDef FurnacePeerGroupDef
	INNER JOIN inserted i ON i.FPGKey = FurnacePeerGroupDef.FPGKey
	
END

GO
CREATE TRIGGER [dbo].[SetFurnacePeerGroupDefModifiedAfterUpdate]
   ON  dbo.FurnacePeerGroupDef
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE FurnacePeerGroupDef
	SET tsModified = getdate()
	FROM dbo.FurnacePeerGroupDef FurnacePeerGroupDef
	INNER JOIN deleted d ON d.FPGKey = FurnacePeerGroupDef.FPGKey
	WHERE d.FurnaceService <> FurnacePeerGroupDef.FurnaceService OR (d.FurnaceService IS NULL AND FurnacePeerGroupDef.FurnaceService IS NOT NULL) OR (d.FurnaceService IS NOT NULL AND FurnacePeerGroupDef.FurnaceService IS NULL)
	OR d.LongText <> FurnacePeerGroupDef.LongText
	OR d.StudyRefnum <> FurnacePeerGroupDef.StudyRefnum
	OR d.StudyUnitID <> FurnacePeerGroupDef.StudyUnitID
	OR d.StudyService <> FurnacePeerGroupDef.StudyService
	
END
