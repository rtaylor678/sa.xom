﻿CREATE TABLE [dbo].[DuplicateRefineryPeers] (
    [SourceRefnum] VARCHAR (25) NOT NULL,
    [CommonRefnum] VARCHAR (25) NOT NULL,
    CONSTRAINT [PK_DuplicateRefineryPeers] PRIMARY KEY CLUSTERED ([SourceRefnum] ASC)
);

