﻿

CREATE VIEW [KNPC].[ProcessPeers]
AS
SELECT pp.* 
FROM dbo.ProcessPeers pp INNER JOIN dbo.ProcessPeerGroupDef pgd ON pgd.PPGKey = pp.FacilityPPGKey
INNER JOIN dbo.CompanyRefnums cr ON cr.Refnum = pgd.StudyRefnum
WHERE cr.CompanyID = 'KNPC'


