﻿
CREATE VIEW [KNPC].[Ranking]
AS
SELECT r.FacilityPGKey, r.PeerGroupPGKey, r.VKey, r.Value, r.DataCount, r.Tile, r.PercentRanking, tsModified
FROM dbo.Ranking r
WHERE r.FacilityPGKey IN (SELECT PGKey FROM KNPC.PeerGroupDef)
AND r.PeerGroupPGKey IN (SELECT PGKey FROM KNPC.PeerGroupDef)


