﻿

CREATE VIEW [KNPC].[RefineryPeers]
AS
SELECT rp.* 
FROM dbo.RefineryPeers rp INNER JOIN dbo.PeerGroupDef pgd ON pgd.PGKey = rp.FacilityPGKey
INNER JOIN dbo.CompanyRefnums cr ON cr.Refnum = pgd.StudyRefnum
WHERE cr.CompanyID = 'KNPC'


