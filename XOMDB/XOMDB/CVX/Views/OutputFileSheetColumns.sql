﻿

CREATE VIEW [CVX].[OutputFileSheetColumns]
AS
SELECT sc.FSKey, sc.ColumnNo, sc.PGKey, sc.PPGKey
FROM dbo.Companies c INNER JOIN dbo.CompanyStudies cs ON cs.CompanyID = c.CompanyID
INNER JOIN dbo.OutputFiles f ON (f.Study = cs.Study) AND f.StudyYear = cs.StudyYear
INNER JOIN dbo.OutputSheets s ON s.FKey = f.FKey
INNER JOIN dbo.OutputSheetColumns sc ON sc.FSKey = s.FSKey
WHERE c.CompanyID = 'CVX' 
AND (cs.tsModified > ISNULL(c.DataPullStartDate,'1/1/2000')
	OR sc.tsModified > ISNULL(c.DataPullStartDate,'1/1/2000') 
	)






