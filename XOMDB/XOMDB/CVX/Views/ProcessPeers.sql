﻿

CREATE VIEW [CVX].[ProcessPeers]
AS
SELECT pp.* 
FROM dbo.ProcessPeers pp INNER JOIN dbo.ProcessPeerGroupDef pgd ON pgd.PPGKey = pp.FacilityPPGKey
INNER JOIN dbo.CompanyRefnums cr ON cr.Refnum = pgd.StudyRefnum
INNER JOIN dbo.Companies c ON c.CompanyID = cr.CompanyID
WHERE c.CompanyID = 'CVX'
AND (pp.tsModified > ISNULL(c.DataPullStartDate, '1/1/2000') 
	OR pgd.tsModified > ISNULL(c.DataPullStartDate, '1/1/2000')
	OR cr.tsModified > ISNULL(c.DataPullStartDate, '1/1/2000')
)



