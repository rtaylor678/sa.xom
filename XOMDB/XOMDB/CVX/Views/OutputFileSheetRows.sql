﻿



CREATE VIEW [CVX].[OutputFileSheetRows]
AS
SELECT r.FSKey, r.RowNo, r.RowText, r.TextIndent, r.Bold, r.NumberFormat, r.SectionCode, r.VKey, r.Hidden
FROM dbo.Companies c INNER JOIN dbo.CompanyStudies cs ON cs.CompanyID = c.CompanyID
INNER JOIN dbo.OutputFiles f ON (f.Study = cs.Study) AND f.StudyYear = cs.StudyYear
INNER JOIN dbo.OutputSheets s ON s.FKey = f.FKey
INNER JOIN dbo.OutputSheetRows r ON r.FSKey = s.FSKey
WHERE c.CompanyID = 'CVX' 
AND (cs.tsModified > ISNULL(c.DataPullStartDate,'1/1/2000')
	OR r.tsModified > ISNULL(c.DataPullStartDate,'1/1/2000')
	)




