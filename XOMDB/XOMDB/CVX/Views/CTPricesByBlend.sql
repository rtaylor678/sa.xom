﻿




CREATE VIEW [CVX].[CTPricesByBlend]
AS
SELECT r.PGKey, r.MaterialCode, r.BlendID, r.MaterialName, r.MaterialType, r.KBbl, r.KMT, r.PricePerBbl, r.PricePerMT, r.ValueMUSD
	, r.Gravity_API, r.Density_KGM3, r.Sulfur_WtPcnt, r.Sulfur_ppm, r.SulfurSpec_ppm, r.RVP_psia, r.RVP_bara, r.Lead_gpg, r.Lead_gpl
	, r.FreezePt_F, r.FreezePt_C, r.CetaneNumber, r.CloudPt_F, r.CloudPt_C, r.Viscosity_CS122F, r.tsModified
FROM dbo.CTPricesByBlend r
WHERE r.PGKey IN (SELECT PGKey FROM dbo.GetPeerGroupDefsNew('CVX',NULL))
AND tsModified >= ISNULL((SELECT DataPullStartDate FROM dbo.Companies WHERE CompanyID = 'CVX'),'1/1/2000')





