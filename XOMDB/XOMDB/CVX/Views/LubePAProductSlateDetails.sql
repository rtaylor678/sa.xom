﻿




CREATE VIEW [CVX].[LubePAProductSlateDetails]
AS
SELECT r.PGKey, r.ReportOrder, r.LWSCategory, r.MaterialID, r.MaterialName, r.Bbl, r.PcntLWSBbl, r.PricePerBbl, r.RMC, r.GrossMargin, r.NWE_150N_GrossMargin, r.GrossMarginOver150N, r.ProdMixVariance, r.tsModified
FROM dbo.LubePAProductSlateDetails r
WHERE r.PGKey IN (SELECT PGKey FROM dbo.GetPeerGroupDefsNew('CVX', NULL))
AND tsModified >= ISNULL((SELECT DataPullStartDate FROM dbo.Companies WHERE CompanyID = 'CVX'),'1/1/2000')







