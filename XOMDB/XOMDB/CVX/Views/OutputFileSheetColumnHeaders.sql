﻿




CREATE VIEW [CVX].[OutputFileSheetColumnHeaders]
AS
SELECT ch.FSKey, ch.ColumnNo, ch.RowNo, ch.CellText, ch.CenterAcrossSelection, ch.LeftBorderLineStyle, ch.LeftBorderWeight, ch.RightBorderLineStyle, ch.RightBorderWeight, ch.TopBorderLineStyle, ch.TopBorderWeight, ch.BottomBorderLineStyle, ch.BottomBorderWeight
FROM dbo.Companies c INNER JOIN dbo.CompanyStudies cs ON cs.CompanyID = c.CompanyID
INNER JOIN dbo.OutputFiles f ON (f.Study = cs.Study) AND f.StudyYear = cs.StudyYear
INNER JOIN dbo.OutputSheets s ON s.FKey = f.FKey
INNER JOIN dbo.OutputSheetColumnHeaders ch ON ch.FSKey = s.FSKey
WHERE c.CompanyID = 'CVX' 
AND (cs.tsModified > ISNULL(c.DataPullStartDate,'1/1/2000')
	OR ch.tsModified > ISNULL(c.DataPullStartDate,'1/1/2000') 
	)



