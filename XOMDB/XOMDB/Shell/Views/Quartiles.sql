﻿CREATE VIEW [Shell].[Quartiles]
AS
SELECT PGKey, VKey, DataCount, NumTiles, Top2, Tile1Break, Tile2Break, Tile3Break, Bottom2, tsModified 
FROM dbo.Quartiles WHERE PGKey IN (SELECT PGKey FROM Shell.PeerGroupDef)
