﻿


CREATE VIEW [Shell].[FurnacePeerGroupDef]
AS
SELECT d.FPGKey, d.PPGKey, d.PGKey, d.ProcessID, d.ProcessType, d.FurnaceService, d.LongText, d.tsModified
FROM dbo.Companies c
CROSS APPLY dbo.GetFurnacePeerGroupDefs(c.CompanyID,c.DataPullStartDate) d
WHERE c.CompanyID = 'Shell'


