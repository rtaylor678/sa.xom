﻿CREATE VIEW [Shell].[PeerGroupDef]
AS
SELECT PGKey, Study, StudyYear, Methodology, RegionCode, MajorGroup, SubGroup
, PricingScenario, LongText, PublishEuro, PGType, tsModified
FROM dbo.Companies c
CROSS APPLY dbo.GetPeerGroupDefsNew(c.CompanyID,c.DataPullStartDate)
WHERE c.CompanyID = 'Shell'



