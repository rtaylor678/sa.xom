﻿CREATE TABLE [XOM].[FurnacePeerGroupDefTemp] (
    [FPGKey]         INT            NOT NULL,
    [PPGKey]         INT            NOT NULL,
    [PGKey]          INT            NOT NULL,
    [ProcessID]      VARCHAR (8)    NOT NULL,
    [ProcessType]    VARCHAR (10)   NOT NULL,
    [FurnaceService] VARCHAR (50)   NULL,
    [LongText]       NVARCHAR (MAX) NOT NULL,
    [tsModified]     DATETIME       NOT NULL
);

