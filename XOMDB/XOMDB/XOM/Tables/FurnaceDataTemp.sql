﻿CREATE TABLE [XOM].[FurnaceDataTemp] (
    [FPGKey]      INT            NOT NULL,
    [VKey]        INT            NOT NULL,
    [numberValue] FLOAT (53)     NULL,
    [textValue]   NVARCHAR (MAX) NULL,
    [tsModified]  DATETIME       NOT NULL
);

