﻿



CREATE VIEW [XOM].[FurnaceData]
AS
SELECT d.FPGKey, d.VKey, d.numberValue, d.textValue, d.tsModified
FROM dbo.Companies c
CROSS APPLY dbo.GetFurnaceData(c.CompanyID,c.DataPullStartDate) d
WHERE c.CompanyID = 'XOM'


