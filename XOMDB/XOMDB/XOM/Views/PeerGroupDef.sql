﻿




CREATE VIEW [XOM].[PeerGroupDef]
AS
SELECT PGKey, Study, StudyYear, Methodology, RegionCode, MajorGroup, SubGroup
, PricingScenario = CASE WHEN Methodology LIKE '%XOM%' AND PricingScenario LIKE 'XOM%' THEN 
		CASE PricingScenario 
			WHEN 'XOMB' THEN 'BASE'
			WHEN 'XOMS' THEN 'SPOT'
			ELSE SUBSTRING(PricingScenario, 4, 5)
		END
	ELSE PricingScenario END
, LongText, PublishEuro, PGType, tsModified
FROM dbo.Companies c
CROSS APPLY dbo.GetPeerGroupDefsNew(c.CompanyID,c.DataPullStartDate)
WHERE c.CompanyID = 'XOM'


