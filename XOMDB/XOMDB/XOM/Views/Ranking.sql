﻿
CREATE VIEW [XOM].[Ranking]
AS
SELECT r.FacilityPGKey, r.PeerGroupPGKey, r.VKey, r.Value, r.DataCount, r.Tile, r.PercentRanking, tsModified
FROM dbo.Ranking r
WHERE r.FacilityPGKey IN (SELECT PGKey FROM dbo.GetPeerGroupDefs('XOM',NULL))
AND r.PeerGroupPGKey IN (SELECT PGKey FROM dbo.GetPeerGroupDefs('XOM',NULL))
AND r.tsModified >= ISNULL((SELECT DataPullStartDate FROM dbo.Companies WHERE CompanyID = 'XOM'),'1/1/2000')

