﻿



CREATE VIEW [XOM].[CTMaterialReports]
AS
SELECT r.PGKey, r.ReportType, r.MaterialKey, r.MaterialCategory, r.MaterialCode, r.MaterialName, r.Barrels, r.MetricTons, r.PricePerBbl, r.PricePerMT, r.ValueMUSD, r.InSensibleHeat, r.IncludeVolumeInMargins, r.CTFCategory, r.tsModified
FROM dbo.CTMaterialReports r
WHERE r.PGKey IN (SELECT PGKey FROM dbo.GetPeerGroupDefsNew('XOM', NULL))
AND tsModified >= ISNULL((SELECT DataPullStartDate FROM dbo.Companies WHERE CompanyID = 'XOM'),'1/1/2000')




