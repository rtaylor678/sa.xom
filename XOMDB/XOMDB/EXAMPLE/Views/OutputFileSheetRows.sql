﻿


CREATE VIEW [EXAMPLE].[OutputFileSheetRows]
AS
SELECT r.FSKey, r.RowNo, r.RowText, r.TextIndent, r.Bold, r.NumberFormat, r.SectionCode, r.VKey
FROM EXAMPLE.OutputFileSheets s
INNER JOIN dbo.OutputSheetRows r ON r.FSKey = s.FSKey
WHERE r.Hidden = 0


