﻿



CREATE VIEW [EXAMPLE].[OutputFileSheetColumnHeaders]
AS
SELECT c.FSKey, c.ColumnNo, c.RowNo, c.CellText, c.CenterAcrossSelection, c.LeftBorderLineStyle, c.LeftBorderWeight, c.RightBorderLineStyle, c.RightBorderWeight, c.TopBorderLineStyle, c.TopBorderWeight, c.BottomBorderLineStyle, c.BottomBorderWeight
FROM EXAMPLE.OutputFileSheets s
INNER JOIN dbo.OutputSheetColumnHeaders c ON c.FSKey = s.FSKey
--INNER JOIN EXAMPLE.OutputFileSheetRows r ON r.FSKey = s.FSKey AND r.RowNo = c.RowNo
--ORDER BY FSKey, ColumnNo, RowNo


